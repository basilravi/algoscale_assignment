import React, { Component } from 'react'
import LoginScreen from './LoginScreen'
import SignupScreen from './SignupScreen'

export default class FirstPage extends Component {
    constructor(props) {
        super(props)
        this.state = {
            isLoginSelected: true
        }
    }
    render() {
        return (
            <div style={{ display:'flex', flex: 1 , flexDirection:'row', height:'100vh'}}>
                <div style={{display:'flex',justifyContent:'center',alignItems:'center', width: '50%'}}>
                    <img style={{ width: '100%' }} alt="Logo" src={require('../logo_page.jpg')} />
                </div>
                <div style={{display:'flex', margin:'5%', width: '50%', flexDirection:'column', justifyContent: 'center', alignItems: 'center'}}>
                    <div style={{display:'flex', width:'100%', flexDirection:'row'}}>
                        <button type="submit"  onClick={() => this.setState({ isLoginSelected: true })}>Login Screen</button>
                        <button type="button" style={{background: 'darkorchid'}} class="cancelbtn"  onClick={() => this.setState({ isLoginSelected: false })}>Signup Screen</button>
                    </div>

                    <div style={{display:'flex', flexDirection:'column'}}>
                        {
                            this.state.isLoginSelected ? <LoginScreen /> : <SignupScreen />
                        }
                    </div>
                </div>


            </div>
        )
    }
}
