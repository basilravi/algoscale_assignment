import React, { Component } from 'react'
import { CRYPTO_API } from '../config'
import crypto from './crypto.json'
import Modal from '../component/Modal'
import { SERVER_API } from '../config'

export default class HomeScreen extends Component {
    constructor(props) {
        super(props)
        this.state = {
            cryptoArray: crypto,
            searchText: '',
            openModal: false,
            showModal: false
        }
        this.onSearchText = this.onSearchText.bind(this)
        this.openModalMethod = this.openModalMethod.bind(this)
    }
    componentDidMount() {
        const options = {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        }
        fetch(CRYPTO_API, options)
            .then(data => {
                console.log(data)
            })
            .catch(err => {
                console.log(err)
            })
    }
    onSearchText(e) {
        this.setState({
            searchText: e.target.value
        })
    }
    openModalMethod() {
        this.setState({
            showModal:!this.state.showModal
        })
    }
    addPairToList = (data) =>{
        let newCrypt = this.state.cryptoArray
        newCrypt[data.name] = data
        this.setState({
            cryptoArray:newCrypt
        },()=>{
            this.addDataToServer(data)
        })
    }

    addDataToServer = (data) =>{
        fetch(SERVER_API + "currency",
            {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(data)
            })
            .then(data => data.json())
            .then(data => {
                console.log("object", data)

            })
            .catch(err => {
                console.log(err)
            })
    }
    render() {

        let rows = [];
        let crypto = this.state.cryptoArray
        for (var key in crypto) {
            if (crypto.hasOwnProperty(key)) {
                let data = crypto[key]
                if (data.name.includes(this.state.searchText) || data.Ticker.includes(this.state.searchText))
                    rows.push(
                        <tr key={key}>
                            <td>{data.name}</td>
                            <td>{data.Ticker}</td>
                            <td>{data.Price}</td>
                            <td><span style={{ color: data.Changes > 0 ? "green" : "red" }}>{data.Changes}</span> </td>
                        </tr>)
            }
        }
        return (
            <div className="container">
                <h1>All Cryptocurrencies</h1>

                <div className="example">
                    <input type="text" placeholder="Search.." onChange={this.onSearchText} name="search2" />
                    <button type="submit"><i className="fa fa-search"></i></button>
                </div>
                <table>
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Pair</th>
                            <th>Price</th>
                            <th>Changes</th>
                        </tr>
                    </thead>
                    <tbody>
                        {rows}
                    </tbody>
                </table>
                <div style={{display:'flex', flexDirection:'row'}}>
                    <div className="add-modal" style={{width:'80%'}}>
                        {this.state.showModal? <Modal addPairToList={this.addPairToList} />: null}
                    </div>
                    <div style={{width:'20%', alignItems:'center', justifyContent:'center'}}>
                        <a className="float" onClick={this.openModalMethod}>
                            <i className="fa fa-plus my-float"></i>
                        </a>

                    </div>

                </div>



            </div>
        )
    }
}
