import React, { Component } from 'react'
import { SERVER_API } from '../config'
import { withRouter } from 'react-router-dom';
import '../App.css'

class SignupScreen extends Component {
  constructor(props) {
    super(props)
    this.state = {
      firstName: '',
      lastName: '',
      email: '',
      password: ''
    }
    this.onSubmitRegistration = this.onSubmitRegistration.bind(this)
  }
  onSubmitRegistration() {
    let data = {
      firstName: this.state.firstName,
      lastName: this.state.lastName,
      email: this.state.email,
      password: this.state.password
    }
    console.log(data)
    fetch(SERVER_API + "user/register",
      {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(data)
      })
      .then(data => data.json())
      .then(data => {
        console.log("object", data)
        this.props.history.push('/home')

      })
      .catch(err => {
        console.log(err)
      })
  }
  onChangeEdit = (ev) => {
    console.log(ev.target.name, ev.target.value)
    this.setState({
      [ev.target.name]: ev.target.value
    })
  }
  render() {
    return (
      <div style={{ display: 'flex', flexDirection: 'column', marginTop: '5%' }}>
        <p>Please fill in this form to create an account.</p>
        <br />
        <div>
          <div>
            <label for="email"><b>First Name</b></label>
            <input type="text" onChange={this.onChangeEdit} value={this.state.firstName} placeholder="Enter First Name" name="firstName" required />

          </div>
          <div></div>
          <label for="email"><b>Enter Last Name</b></label>
          <input type="text" onChange={this.onChangeEdit} placeholder="Enter Last Name" name="lastName" required />

        </div>

        <label for="email"><b>Email</b></label>
        <input type="text" onChange={this.onChangeEdit} placeholder="Enter Email" name="email" required />

        <label for="psw"><b>Password</b></label>
        <input type="password" onChange={this.onChangeEdit} placeholder="Enter Password" name="password" required />

        <p>By creating an account you agree to our Terms & Privacy.</p>

        <div className="clearfix">
          <button type="submit" className="signupbtn" onClick={this.onSubmitRegistration}>Sign Up</button>
        </div>
      </div>

    )
  }
}

export default withRouter(SignupScreen)