import React from "react";
import { BrowserRouter as Router, Route,Switch} from "react-router-dom";
import SignupScreen from './SignupScreen'
import HomeScreen from './HomeScreen'
import FirstPage from './FirstPage'

const AppRouter = () => (
    <Router>
        <Switch>
            <Route path="/" exact component={FirstPage} />
            <Route path="/signup/" component={SignupScreen} />
            <Route path="/home/" component={HomeScreen} />
        </Switch>
    </Router>
  );
  
  export default AppRouter;
  