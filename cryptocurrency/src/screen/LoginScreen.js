import React, { Component } from 'react'
import { SERVER_API } from '../config'
import { withRouter } from 'react-router-dom';
import '../App.css'
class LoginScreen extends Component {
    constructor(props) {
        super(props)
        this.state = {
            psw: '123456',
            uname: 'ravi@gmail.com'
        }
        this.onSubmitLogin = this.onSubmitLogin.bind(this)
        this.onChangeInput = this.onChangeEdit.bind(this)
    }
    onSubmitLogin() {
        let data = {
            email: this.state.uname,
            password: this.state.psw
        }
        console.log(data)
        fetch(SERVER_API + "user/login",
            {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(data)
            })
            .then(data => data.json())
            .then(data => {
                console.log("object", data)
                this.props.history.push('/home')

            })
            .catch(err => {
                console.log(err)
            })
    }
    onChangeEdit = (ev) => {
        this.setState({
            [ev.target.name]: ev.target.value
        })
    }
    render() {
        return (
            <div style={{marginTop:'10%'}}>
                <label for="uname"><b>Username</b></label>
                <input type="text" placeholder="Enter Username" name="uname" onChange={this.onChangeEdit} />

                <label for="psw"><b>Password</b></label>
                <input type="password" placeholder="Enter Password" name="psw" onChange={this.onChangeEdit} required />
                <div style={{display:'flex', flexDirection:'column'}}>
                    <button type="submit" onClick={this.onSubmitLogin}>Login</button>
                    <br />
                    <button type="button" class="cancelbtn" onClick={() => this.props.history.push('/home')}>Cancel</button>
                </div>

            </div>

        )
    }
}
export default withRouter(LoginScreen)