import React, { Component } from 'react'

export default class Modal extends Component {
  constructor(props){
    super(props)
    this.state={
      name:'',
      pair:'',
      price:0,
      changes:0,
      market:0
    }
  }
  addPairToList = () =>{
    if(this.state.name.length === 0)
      return false
    if(this.state.pair.length === 0)
      return false
    let data = {
        Ticker: this.state.pair,
        Price: this.state.price,
        Changes: this.state.changes,
        name: this.state.name,
        market_cap_usd: this.state.market
    }
    this.props.addPairToList(data)
  }
  onChange = (ev) =>{
    this.setState({
      [ev.target.name]:ev.target.value
    })
  }
  render() {
    return (
        <div style={{display:'flex', flexDirection:'row'}}>
            <input style={{width: '17%',margin:'10px'}} onChange={this.onChange} name="name" type="text" placeholder="Name"/>
            <input style={{width: '17%',margin:'10px'}} onChange={this.onChange} name="pair" type="text" placeholder="Pair"/>
            <input style={{width: '17%',margin:'10px'}} onChange={this.onChange} name="price" type="text" placeholder="Price"/>
            <input style={{width: '17%',margin:'10px'}} onChange={this.onChange} name="changes" type="text" placeholder="Changes"/>
            <input style={{width: '17%',margin:'10px'}} onChange={this.onChange} name="market" type="text" placeholder="Market"/>
            <input style={{ width: '10%',margin:'10px'}} onClick={this.addPairToList} type="submit"></input>
        </div>
    )
  }
}
