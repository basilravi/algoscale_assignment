
const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const http = require('http');
const app = express();
const cors = require('cors');
const config = require('./config')

app.use(cors())

app.use(bodyParser.urlencoded({ extended: true}));
app.use(bodyParser.json());

//Initialize App configuration

mongoose.set('useCreateIndex', true)
mongoose.connect(config.url,{ useNewUrlParser: true })
.then(() => {
  console.log("Successfully connected to the database");
})
.catch(err => {
  console.log('Could not connect to the database. Exiting now...');
    process.exit();  // to close app Completely 
})

app.get('/', (req, res) => {
  
  res.json({"message": "Welcome to Crypto"});
});

require('./app/routes/user.routes')(app);
require('./app/routes/currency.routes')(app);

var httpServer = http.createServer(app);
httpServer.listen(8443, () => {
  console.log("Server is listening on port 8443");
});