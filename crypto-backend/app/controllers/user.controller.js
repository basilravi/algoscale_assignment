const User = require('../models/user.model');

exports.registerUser = (req, res) => {
    console.log(req.body)

    if (req.body.password != undefined) {
        const user = new User({
            firstName: req.body.firstName,
            lastName: req.body.lastName,
            email: req.body.email,
            password: req.body.password
        })
        user.save()
            .then(user => {
                if (!user || user == null) {
                    return res.status(200).send({
                        response: 'User not added.',
                    });
                }
                res.status(200).send({ data: user, message: "User created Successfully" });
            })
            .catch(err => {
                let errorObject = error.getErrorMessage(err)
                res.status(errorObject.code).send({ message: errorObject.message })
            })
    } else {
        res.status(500).send('Empty password')
    }
}
//login user
exports.loginUser = (req, res) => {
    //Validation required

    User.findOne({ email: req.body.email })
        .then(user => {
            if (!user || user == null) {
                return res.status(200).send({
                    response: 'User not present.',
                    auth: false,
                    token: null
                });
            }
            res.status(200).send({ auth: true ,user });

        })
        .catch(err => {
            let errorObject = error.getErrorMessage(err)
            res.status(errorObject.code).send({ message: errorObject.message })
        })
}

//get one
exports.getOne = (req, res) => {
    User.findById(req.params.id)
        .then(user => {
            if (!user || data == null) {
                res.status(200).send({
                    message: "Record not found"
                })
            }
            res.status(200).send({
                message: "Record found successfully",
                data: user
            })
        })
        .catch(err => {
            let errorObject = error.getErrorMessage(err)
            res.status(errorObject.code).send({ message: errorObject.message })
        })
}

//update details
exports.update = (req, res) => {
    if (req.body.bidieyeUserPassword != undefined) {
        let hashedPassword = bcrypt.hashSync(req.body.bidieyeUserPassword, 8);

         const data = {
            firstName: req.body.firstName,
            lastName: req.body.lastName,
            email: req.body.email,
            password: req.body.password
        }
        User.findByIdAndUpdate(req.params.id, { $set: data }, { new: true })
            .then(user => {
                if (!user || user == null) {
                    res.status(200).send({
                        message: "Record not found"
                    })
                }
                res.status(200).send({
                    message: "Record updated successfully",
                    data: user
                })
            })
            .catch(err => {
                let errorObject = error.getErrorMessage(err)
                res.status(errorObject.code).send({ message: errorObject.message })
            })
    } else {
        res.status(500).send('Empty password')
    }
}
//delete the user
exports.delete = (req, res) => {
    User.findByIdAndRemove(req.params.id)
        .then(user => {
            if (!user || user == null) {
                res.status(200).send({
                    message: "Record not found"
                })
            }
            res.status(200).send({
                message: "Record deleted successfully",
            })
        })
        .catch(err => {
            let errorObject = error.getErrorMessage(err)
            res.status(errorObject.code).send({ message: errorObject.message })
        })
}

//getAllUser
exports.getAll = (req, res) => {
    BidieyeUser.find()
        .then(users => {
            if (!users || users == null) {
                res.status(200).send({
                    message: "Record not found"
                })
            }
            res.status(200).send({
                message: "Record fetched successfully",
                data: users
            })
        })
        .catch(err => {
            let errorObject = error.getErrorMessage(err)
            res.status(errorObject.code).send({ message: errorObject.message })
        })
}