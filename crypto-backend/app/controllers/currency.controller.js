const CurrencySchema = require('../models/currency.model');

exports.createCurrency = (req, res) => {
        const currencySchema = new CurrencySchema({
            user_id:req.body.user_id,
            name: req.body.name,
            symbol: req.body.symbol,
            price: req.body.price,
            changes: req.body.changes
        })
        currencySchema.save()
            .then(user => {
                if (!user || user == null) {
                    return res.status(200).send({
                        response: 'User not added.',
                    });
                }
                res.status(200).send({ data: user, message: "Record created Successfully" });
            })
            .catch(err => {
                let errorObject = error.getErrorMessage(err)
                res.status(errorObject.code).send({ message: errorObject.message })
            })
}

//get one
exports.getOne = (req, res) => {
    CurrencySchema.findById(req.params.id)
        .then(data => {
            if (!data || data == null) {
                res.status(200).send({
                    message: "Record not found"
                })
            }
            res.status(200).send({
                message: "Record found successfully",
                data: data
            })
        })
        .catch(err => {
            res.status(500).send({ message: err })
        })
}

//update details
exports.update = (req, res) => {
    if (req.body.bidieyeUserPassword != undefined) {
        let hashedPassword = bcrypt.hashSync(req.body.bidieyeUserPassword, 8);

         const data = {
            firstName: req.body.firstName,
            lastName: req.body.lastName,
            email: req.body.email,
            password: req.body.password
        }
        User.findByIdAndUpdate(req.params.id, { $set: data }, { new: true })
            .then(user => {
                if (!user || user == null) {
                    res.status(200).send({
                        message: "Record not found"
                    })
                }
                res.status(200).send({
                    message: "Record updated successfully",
                    data: user
                })
            })
            .catch(err => {
                let errorObject = error.getErrorMessage(err)
                res.status(errorObject.code).send({ message: errorObject.message })
            })
    } else {
        res.status(500).send('Empty password')
    }
}
//delete the user
exports.delete = (req, res) => {
    User.findByIdAndRemove(req.params.id)
        .then(user => {
            if (!user || user == null) {
                res.status(200).send({
                    message: "Record not found"
                })
            }
            res.status(200).send({
                message: "Record deleted successfully",
            })
        })
        .catch(err => {
            res.status(500).send({ message: err })
        })
}

//getAll Currency
exports.getAll = (req, res) => {
    CurrencySchema.find()
        .then(data => {
            if (!data || data == null) {
                res.status(200).send({
                    message: "Record not found"
                })
            }
            res.status(200).send({
                message: "Record fetched successfully",
                data: data
            })
        })
        .catch(err => {
            res.status(500).send({ message: err })
        })
}