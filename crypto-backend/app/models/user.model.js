//provide all package roles and authentication 
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const UserSchema =  mongoose.Schema({
    firstName:{ type: String},
    lastName:{ type: String},
    email:{ type: String, unique: true, required: true},
    password: { type: String }
},{
    timestamps: true
})
module.exports = mongoose.model('UserSchema',UserSchema)
