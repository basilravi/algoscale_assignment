//provide all package roles and authentication 
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const CurrencySchema =  mongoose.Schema({
    user_id:{type: String},
    name:{ type: String,unique: true, required: true},
    symbol:{ type: String},
    price:{ type: String},
    changes: { type: String }
},{
    timestamps: true
})
module.exports = mongoose.model('CurrencySchema',CurrencySchema)
