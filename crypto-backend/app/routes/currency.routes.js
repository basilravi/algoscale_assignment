
const currency = require('../controllers/currency.controller');

module.exports = (app) => {

  //register user
  app.post('/currency', currency.createCurrency);

  //update user
  app.put('/currency', currency.update);
  
  //delete user
  app.delete('/currency', currency.delete);

  //get single user
  app.get('/currency', currency.getOne);

  //get all user
  app.get('/currency', currency.getAll);



}
