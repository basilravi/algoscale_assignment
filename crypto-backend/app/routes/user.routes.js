
const user = require('../controllers/user.controller');

module.exports = (app) => {

  //register user
  app.post('/user/register', user.registerUser);

  //login
  app.post('/user/login', user.loginUser);

  //update user
  app.put('/user/:id', user.update);
  
  //delete user
  app.delete('/user/:id', user.delete);

  //get single user
  app.get('/user/:id', user.getOne);

  //get all user
  app.get('/user', user.getAll);



}
